package pl.tomczyk.arkadiusz.platerecognition.dao.entity;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.LocalDate;


@Entity
@Table(name = "RECOGNITION_PLATES")
public class RegistrationPlateEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "RECOGNITION_PLATE_ID_SEQ")
    @SequenceGenerator(name = "RECOGNITION_PLATE_ID_SEQ", sequenceName = "RECOGNITION_PLATE_ID_SEQ")
    private Long id;
    @Column(name="NUMBERS")
    private String numbers;
    @Column(name="CREATED")
    private LocalDate created;

    @ManyToOne
    @JoinColumn(name = "LOCATION")
    private LocationEntity locationEntity;

    @Lob
    @Basic(fetch = FetchType.LAZY)
    private byte[] image;


    public RegistrationPlateEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumbers() {
        return numbers;
    }

    public void setNumbers(String numbers) {
        this.numbers = numbers;
    }

    public LocationEntity getLocationEntity() {
        return locationEntity;
    }

    public void setLocationEntity(LocationEntity locationEntity) {
        this.locationEntity = locationEntity;
    }

    public LocalDate getCreated() {
        return created;
    }

    public void setCreated(LocalDate created) {
        this.created = created;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "RegistrationPlateEntity{" +
                "id=" + id +
                ", numbers='" + numbers + '\'' +
                ", created=" + created +
                ", location=" + locationEntity +
                '}';
    }
}
