package pl.tomczyk.arkadiusz.platerecognition.dao.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "VEHICLE")
public class VehicleEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "VEHICLE_ID_SEQ")
    @SequenceGenerator(name = "VEHICLE_ID_SEQ", sequenceName = "VEHICLE_ID_SEQ")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "RECOGNITION_PLATE")
    private RegistrationPlateEntity plate;

    @ManyToOne
    @JoinColumn(name = "LOCATION")
    private LocationEntity locationEntity;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocationEntity getLocationEntity() {
        return locationEntity;
    }

    public void setLocationEntity(LocationEntity locationEntity) {
        this.locationEntity = locationEntity;
    }

    public RegistrationPlateEntity getPlate() {
        return plate;
    }

    public void setPlate(RegistrationPlateEntity plate) {
        this.plate = plate;
    }

    @Override
    public String toString() {
        return "VehicleEntity{" +
                "location=" + locationEntity +
                ", plate=" + plate +
                '}';
    }
}
