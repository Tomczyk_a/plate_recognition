package pl.tomczyk.arkadiusz.platerecognition.dao.entity;

import lombok.Data;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import static javax.persistence.FetchType.LAZY;

@Entity
@Table(name = "FILES_UPLOAD")
@Data
public class UploadFile {
    @Id
    @GeneratedValue
    @Column(name = "FILE_ID")
    private long id;
    @Column(name = "FILE_NAME")
    private String fileName;
    @Lob
    @Basic(fetch=LAZY)
    @Column(name = "FILE_DATA")
    private byte[] data;
}
