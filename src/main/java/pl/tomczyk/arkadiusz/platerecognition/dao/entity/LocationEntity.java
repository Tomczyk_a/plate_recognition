package pl.tomczyk.arkadiusz.platerecognition.dao.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "LOCATION")

public class LocationEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LOCATION_ID_SEQ")
    @SequenceGenerator(name = "LOCATION_ID_SEQ", sequenceName = "LOCATION_ID_SEQ")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "GPS")
    private GPSCoordinatesEntity gps;

    @ManyToOne
    @JoinColumn(name = "address_entity")
    private AddressEntity addressEntity;

    public LocationEntity(GPSCoordinatesEntity gps, AddressEntity addressEntity) {
        this.gps = gps;
        this.addressEntity = addressEntity;
    }

    public LocationEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public GPSCoordinatesEntity getGps() {
        return gps;
    }

    public void setGps(GPSCoordinatesEntity gps) {
        this.gps = gps;
    }


    public AddressEntity getAddressEntity() {
        return addressEntity;
    }

    public void setAddressEntity(AddressEntity addressEntity) {
        this.addressEntity = addressEntity;
    }

    @Override
    public String toString() {
        return "LocationEntity{" +
                "gps=" + gps +
                ", addressEntity=" + addressEntity +
                '}';
    }
}
