package pl.tomczyk.arkadiusz.platerecognition.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "ADDRESSES")
public class AddressEntity {
    //todo zmana nazw pozostałych klas w entity oraz przypisanie nazw tabel
    // todo identity zmienić na sequence - zrobic w kazdej klasie

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "ADDRESS_ID_SEQ")
    @SequenceGenerator(name = "ADDRESS_ID_SEQ", sequenceName = "ADDRESS_ID_SEQ")
    private Long id;
    @Column(name="NUMBER")
    private String number;
    @Column(name="POST_CODE")
    private String postCode;
    @Column(name="NAME")
    private String name;

    public AddressEntity() {
    }

    public AddressEntity(String number, String postCode, String name) {
        this.number = number;
        this.postCode = postCode;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPostCode() {
        return postCode;
    }

    public void setPostCode(String postCode) {
        this.postCode = postCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "AddressEntity{" +
                "number='" + number + '\'' +
                ", postCode='" + postCode + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
