package pl.tomczyk.arkadiusz.platerecognition.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.AddressEntity;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {


}
