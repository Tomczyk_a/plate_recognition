package pl.tomczyk.arkadiusz.platerecognition.dao.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "GPS_COORDINATES")
public class GPSCoordinatesEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "GPS_COORDINATES_ID_SEQ")
    @SequenceGenerator(name = "GPS_COORDINATES_ID_SEQ", sequenceName = "GPS_COORDINATES_ID_SEQ")
    private Long id;
    @Column(name="LATITUDE")
    private Long longitude;
    @Column(name="LONGITUDE")
    private Long latitude;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLongitude() {
        return longitude;
    }

    public void setLongitude(Long longitude) {
        this.longitude = longitude;
    }

    public Long getLatitude() {
        return latitude;
    }

    public void setLatitude(Long latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "GPSCoordinatesEntity{" +
                "longitude=" + longitude +
                ", latitude=" + latitude +
                '}';
    }
}
