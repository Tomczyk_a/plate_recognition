package pl.tomczyk.arkadiusz.platerecognition.dao.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.RegistrationPlateEntity;

@Repository
public interface RegistrationPlateRepository extends JpaRepository <RegistrationPlateEntity, Long> {
}
