package pl.tomczyk.arkadiusz.platerecognition.web.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GPSCoordinatesModel {

    private Long id;
    private Long longitude;
    private Long latitude;


}
