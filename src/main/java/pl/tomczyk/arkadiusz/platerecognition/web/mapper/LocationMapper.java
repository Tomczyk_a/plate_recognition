package pl.tomczyk.arkadiusz.platerecognition.web.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.LocationEntity;
import pl.tomczyk.arkadiusz.platerecognition.web.model.LocationModel;

@Component
public class LocationMapper {

    public LocationEntity fromModel(LocationModel locationModel) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(locationModel, LocationEntity.class);
    }

    public LocationModel fromEntity(LocationEntity locationEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(locationEntity, LocationModel.class);
    }
}
