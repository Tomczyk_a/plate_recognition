package pl.tomczyk.arkadiusz.platerecognition.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate.RegistrationPlateException;
import pl.tomczyk.arkadiusz.platerecognition.service.PlateRecognitionService;
import pl.tomczyk.arkadiusz.platerecognition.web.model.PlateRecognizerRequestModel;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateModel;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateResponseModel;

import java.io.IOException;
import java.time.LocalDate;


@RestController
@RequestMapping(value = "/plate-recognition")
public class PlateRecognitionRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlateRecognitionRestController.class);

    @Autowired
    private PlateRecognitionService plateRecognitionService;

    @GetMapping("/dummy")
    public RegistrationPlateModel dummy(){
        RegistrationPlateModel registrationPlateModel = new RegistrationPlateModel();
        registrationPlateModel.setNumbers("wm 12345");
        LOGGER.info("dummy");
        return registrationPlateModel;

    }

    @PostMapping("/recognition")
    @ResponseStatus(HttpStatus.CREATED)
    public RegistrationPlateResponseModel recognition (@RequestParam("file") MultipartFile file) throws IOException, RegistrationPlateException {
        LOGGER.info("recognition ");
        RegistrationPlateModel registrationPlateModel = new RegistrationPlateModel();
        registrationPlateModel.setImage(file.getBytes());

        PlateRecognizerRequestModel plateRecognizerRequestModel = PlateRecognizerRequestModel.builder()
                .fileName(file.getOriginalFilename())
                .content(file.getBytes())
                .created(LocalDate.now())
                .resource(file.getResource())
                .build();

        Resource resource = file.getResource();

//        return plateRecognitionService.recognition(registrationPlateModel, resource);
        return plateRecognitionService.recognition(plateRecognizerRequestModel);
    }
}
