package pl.tomczyk.arkadiusz.platerecognition.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.LocationEntity;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegistrationPlateResponseModel {
    private String numbers;
    private LocalDate created;
    private LocationEntity location;
}
