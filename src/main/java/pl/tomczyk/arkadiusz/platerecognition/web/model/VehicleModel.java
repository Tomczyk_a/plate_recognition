package pl.tomczyk.arkadiusz.platerecognition.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.LocationEntity;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.RegistrationPlateEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VehicleModel {

    private Long id;
    private RegistrationPlateEntity plate;
    private LocationEntity locationEntity;
}
