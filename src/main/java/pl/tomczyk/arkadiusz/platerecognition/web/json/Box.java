
package pl.tomczyk.arkadiusz.platerecognition.web.json;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "ymin",
    "xmin",
    "ymax",
    "xmax"
})
public class Box {

    @JsonProperty("ymin")
    private Integer ymin;
    @JsonProperty("xmin")
    private Integer xmin;
    @JsonProperty("ymax")
    private Integer ymax;
    @JsonProperty("xmax")
    private Integer xmax;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("ymin")
    public Integer getYmin() {
        return ymin;
    }

    @JsonProperty("ymin")
    public void setYmin(Integer ymin) {
        this.ymin = ymin;
    }

    @JsonProperty("xmin")
    public Integer getXmin() {
        return xmin;
    }

    @JsonProperty("xmin")
    public void setXmin(Integer xmin) {
        this.xmin = xmin;
    }

    @JsonProperty("ymax")
    public Integer getYmax() {
        return ymax;
    }

    @JsonProperty("ymax")
    public void setYmax(Integer ymax) {
        this.ymax = ymax;
    }

    @JsonProperty("xmax")
    public Integer getXmax() {
        return xmax;
    }

    @JsonProperty("xmax")
    public void setXmax(Integer xmax) {
        this.xmax = xmax;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
