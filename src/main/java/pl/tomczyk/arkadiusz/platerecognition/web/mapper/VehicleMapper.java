package pl.tomczyk.arkadiusz.platerecognition.web.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.VehicleEntity;
import pl.tomczyk.arkadiusz.platerecognition.web.model.VehicleModel;

@Component
public class VehicleMapper {

    public VehicleEntity fromModel(VehicleModel vehicleModel){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(vehicleModel, VehicleEntity.class);
    }

    public VehicleModel fromEntity(VehicleEntity vehicleEntity) {
        ModelMapper modelMapper= new ModelMapper();
        return modelMapper.map(vehicleEntity, VehicleModel.class);
    }
}
