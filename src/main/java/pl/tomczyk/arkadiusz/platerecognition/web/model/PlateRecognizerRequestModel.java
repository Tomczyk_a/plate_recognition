package pl.tomczyk.arkadiusz.platerecognition.web.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.core.io.Resource;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlateRecognizerRequestModel {
    private LocalDate created;
    private Resource resource;
    private String fileName;
    private byte[] content;
    private String numbers;

}
