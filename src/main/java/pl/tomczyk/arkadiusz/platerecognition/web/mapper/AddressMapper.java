package pl.tomczyk.arkadiusz.platerecognition.web.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.AddressEntity;
import pl.tomczyk.arkadiusz.platerecognition.web.model.AddressModel;

@Component
public class AddressMapper {

    public AddressEntity fromModel(AddressModel addressModel){
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(addressModel, AddressEntity.class);
    }

    public AddressModel fromEntity(AddressEntity addressEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(addressEntity, AddressModel.class);
    }

}
