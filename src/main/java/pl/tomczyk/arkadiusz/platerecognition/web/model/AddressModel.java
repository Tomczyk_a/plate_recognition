package pl.tomczyk.arkadiusz.platerecognition.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddressModel {

    private Long id;
    private String number;
    private String postCode;
    private String name;

}
