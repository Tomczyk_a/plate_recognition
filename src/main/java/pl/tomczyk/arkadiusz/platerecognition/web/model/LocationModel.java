package pl.tomczyk.arkadiusz.platerecognition.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.AddressEntity;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.GPSCoordinatesEntity;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationModel {

    private Long id;
    private GPSCoordinatesEntity gps;
    private AddressEntity addressEntity;
}
