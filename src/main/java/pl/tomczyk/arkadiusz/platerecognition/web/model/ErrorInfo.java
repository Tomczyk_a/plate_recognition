package pl.tomczyk.arkadiusz.platerecognition.web.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorInfo {
    private String url;
    private String message;
    private LocalDateTime occurred;
}
