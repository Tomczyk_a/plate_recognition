package pl.tomczyk.arkadiusz.platerecognition.web.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.GPSCoordinatesEntity;
import pl.tomczyk.arkadiusz.platerecognition.web.model.GPSCoordinatesModel;

@Component
public class GPSCoordinatesMapper {

    public GPSCoordinatesEntity fromModel(GPSCoordinatesModel gpsCoordinatesModel) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(gpsCoordinatesModel, GPSCoordinatesEntity.class);
    }

    public GPSCoordinatesModel fromEntity(GPSCoordinatesEntity gpsCoordinatesEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(gpsCoordinatesEntity, GPSCoordinatesModel.class);
    }

}
