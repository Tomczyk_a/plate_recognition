package pl.tomczyk.arkadiusz.platerecognition.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pl.tomczyk.arkadiusz.platerecognition.service.RegistrationPlateService;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateModel;

import java.util.List;

@RestController
@RequestMapping(value = "/registration-plate")
public class RegistrationPlateRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(RegistrationPlateRestController.class);

    @Autowired
    private RegistrationPlateService registrationPlateService;

    @GetMapping
    public List<RegistrationPlateModel> list() {
        return registrationPlateService.list();
    }

    @PostMapping
    public RegistrationPlateModel create(@RequestParam(name = "registrationNumber") String registrationNumber,
                                         @RequestParam("file") MultipartFile file){
        LOGGER.info("#####$$$#### registrationNumber:  " + registrationNumber);
        LOGGER.info("#####$$$#### file:  " + file.getOriginalFilename());

        RegistrationPlateModel registrationPlateModel = new RegistrationPlateModel();
        registrationPlateModel.setNumbers(registrationNumber);
        return registrationPlateModel;
    }

}
