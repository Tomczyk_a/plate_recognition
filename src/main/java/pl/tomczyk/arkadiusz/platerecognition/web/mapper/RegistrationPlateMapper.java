package pl.tomczyk.arkadiusz.platerecognition.web.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.RegistrationPlateEntity;
import pl.tomczyk.arkadiusz.platerecognition.web.model.PlateRecognizerRequestModel;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateModel;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RegistrationPlateMapper {



    public RegistrationPlateEntity fromModel(RegistrationPlateModel registrationPlateModel) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(registrationPlateModel, RegistrationPlateEntity.class);
    }

    public RegistrationPlateModel fromEntity(RegistrationPlateEntity registrationPlateEntity) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(registrationPlateEntity, RegistrationPlateModel.class);
    }

    public List<RegistrationPlateModel> fromEntityList(List<RegistrationPlateEntity> registrationPlateEntities){
       return registrationPlateEntities.stream()
               .map(this::fromEntity)
//               .map(entity -> fromEntity(entity))
               .collect(Collectors.toList());

    }

    public RegistrationPlateModel fromRequest(PlateRecognizerRequestModel plateRecognizerRequestModel) {
        RegistrationPlateModel registrationPlateModel = new RegistrationPlateModel();
        registrationPlateModel.setCreated(plateRecognizerRequestModel.getCreated());
        registrationPlateModel.setImage(plateRecognizerRequestModel.getContent());
        registrationPlateModel.setNumbers(plateRecognizerRequestModel.getNumbers());
        return registrationPlateModel;
    }

}
