package pl.tomczyk.arkadiusz.platerecognition.web.mapper;

import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;
import pl.tomczyk.arkadiusz.platerecognition.web.json.PlateRecognizerResponse;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateModel;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateResponseModel;

@Component
public class RegistrationPlateResponseMapper {


    public RegistrationPlateResponseModel toResponse(RegistrationPlateModel registrationPlateModel) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(registrationPlateModel, RegistrationPlateResponseModel.class);
    }

    public RegistrationPlateResponseModel fromPlateRecognizer(PlateRecognizerResponse plateRecognizerResponse) {
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(plateRecognizerResponse, RegistrationPlateResponseModel.class);

    }

}
