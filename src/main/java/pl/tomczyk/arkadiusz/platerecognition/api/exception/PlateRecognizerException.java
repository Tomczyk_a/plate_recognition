package pl.tomczyk.arkadiusz.platerecognition.api.exception;

import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus
public class PlateRecognizerException extends Exception {

    public PlateRecognizerException(String message) {
        super(message);
    }

    public PlateRecognizerException(String message, Throwable cause) {
        super(message, cause);
    }
}
