package pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import pl.tomczyk.arkadiusz.platerecognition.web.model.ErrorInfo;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@ControllerAdvice
public class GlobalControllerAdvice {

    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalControllerAdvice.class);

    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler({RegistrationPlateException.class})
    private ErrorInfo handleRecognitionException(Exception e, HttpServletRequest request){
        LOGGER.warn("Handling exception");
        return new ErrorInfo(request.getRequestURI(), e.getMessage(), LocalDateTime.now());
    }


}
