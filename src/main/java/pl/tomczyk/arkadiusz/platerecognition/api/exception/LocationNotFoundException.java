package pl.tomczyk.arkadiusz.platerecognition.api.exception;

public class LocationNotFoundException extends NotFoundException {

    public LocationNotFoundException(String message) {
        super(message);
    }

    public LocationNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
