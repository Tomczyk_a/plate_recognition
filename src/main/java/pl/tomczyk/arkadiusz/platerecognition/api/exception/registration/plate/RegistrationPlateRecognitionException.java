package pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate;

//@ResponseStatus(HttpStatus.SERVICE_UNAVAILABLE)
public class RegistrationPlateRecognitionException extends RegistrationPlateException {

    public RegistrationPlateRecognitionException(String message) {
        super(message);
    }

    public RegistrationPlateRecognitionException(String message, Throwable cause) {
        super(message, cause);
    }
}
