package pl.tomczyk.arkadiusz.platerecognition.api.exception;

public class VehicleNotFoundException extends Exception {

    public VehicleNotFoundException(String message) {
        super(message);
    }
}
