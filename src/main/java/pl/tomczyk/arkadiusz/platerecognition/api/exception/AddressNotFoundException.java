package pl.tomczyk.arkadiusz.platerecognition.api.exception;

public class AddressNotFoundException extends Exception {

    public AddressNotFoundException(String message) {
        super(message);
    }
}
