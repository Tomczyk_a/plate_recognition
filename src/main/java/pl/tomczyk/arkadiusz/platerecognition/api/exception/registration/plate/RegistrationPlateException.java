package pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate;

public class RegistrationPlateException extends Exception {

    public RegistrationPlateException(String message) {
        super(message);
    }

    public RegistrationPlateException(String message, Throwable cause) {
        super(message, cause);
    }
}
