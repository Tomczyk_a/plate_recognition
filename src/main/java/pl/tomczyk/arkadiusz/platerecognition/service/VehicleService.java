package pl.tomczyk.arkadiusz.platerecognition.service;

import pl.tomczyk.arkadiusz.platerecognition.api.exception.VehicleNotFoundException;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.VehicleEntity;
import pl.tomczyk.arkadiusz.platerecognition.dao.repositories.VehicleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.tomczyk.arkadiusz.platerecognition.web.mapper.VehicleMapper;
import pl.tomczyk.arkadiusz.platerecognition.web.model.VehicleModel;

import java.util.Optional;

@Service
public class VehicleService {

    @Autowired
    private VehicleRepository vehicleRepository;

    @Autowired
    private VehicleMapper vehicleMapper;

    public VehicleModel create(VehicleModel vehicleModel) {
        VehicleEntity vehicleEntity = vehicleMapper.fromModel(vehicleModel);
        VehicleEntity savedVehicleEntity = vehicleRepository.save(vehicleEntity);
        return vehicleMapper.fromEntity(savedVehicleEntity);
    }

    public VehicleModel read(Long id) throws VehicleNotFoundException {
        Optional<VehicleEntity> optionalVehicleEntity = vehicleRepository.findById(id);
        VehicleEntity vehicleEntity = optionalVehicleEntity.orElseThrow(() -> new VehicleNotFoundException("unable to find vehicle by Id: " + id));
        return vehicleMapper.fromEntity(vehicleEntity);
    }

}
