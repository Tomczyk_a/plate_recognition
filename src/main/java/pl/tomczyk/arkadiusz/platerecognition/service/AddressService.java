package pl.tomczyk.arkadiusz.platerecognition.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.AddressNotFoundException;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.AddressEntity;
import pl.tomczyk.arkadiusz.platerecognition.dao.repositories.AddressRepository;
import pl.tomczyk.arkadiusz.platerecognition.web.mapper.AddressMapper;
import pl.tomczyk.arkadiusz.platerecognition.web.model.AddressModel;

import java.util.Optional;

@Service
public class AddressService {

    @Autowired
    private AddressMapper addressMapper;

    @Autowired
    private AddressService addressService;

    @Autowired
    private AddressRepository addressRepository;

    public AddressModel create(AddressModel addressModel) {
        AddressEntity addressEntity = addressMapper.fromModel(addressModel);
        AddressEntity saveAddressEntity = addressRepository.save(addressEntity);
        return addressMapper.fromEntity(saveAddressEntity);
    }

    public AddressModel read(Long id) throws AddressNotFoundException {
        Optional<AddressEntity> optionalAddressEntity = addressRepository.findById(id);
        AddressEntity addressEntity = optionalAddressEntity.orElseThrow(() -> new AddressNotFoundException("unable to find address by Id: " + id));
        return addressMapper.fromEntity(addressEntity);
    }

}
