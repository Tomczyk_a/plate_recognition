package pl.tomczyk.arkadiusz.platerecognition.service;

import org.springframework.core.io.Resource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.PlateRecognizerException;
import pl.tomczyk.arkadiusz.platerecognition.web.json.PlateRecognizerResponse;
import pl.tomczyk.arkadiusz.platerecognition.web.json.Result;

import java.util.List;

@Service
public class PlateRecognizerService {
    //
    public String plateRecognizer(Resource resource) throws PlateRecognizerException {
        String resultPlate = null;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.MULTIPART_FORM_DATA);
            headers.add("Authorization", "Token 6b909e4536848a85d5c69ce7c4d7fa74a13a71ee");

            MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();
            body.add("upload", resource);

            HttpEntity<MultiValueMap<String, Object>> requestEntity = new HttpEntity<>(body, headers);

            String serverUrl = "https://api.platerecognizer.com/v1/plate-reader/";

            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<PlateRecognizerResponse> response = restTemplate.exchange(
                    serverUrl, HttpMethod.POST, requestEntity, PlateRecognizerResponse.class);

            if (response != null) {
                PlateRecognizerResponse responseBody = response.getBody();
                if (responseBody != null) {
                    List<Result> results = responseBody.getResults();
                    if (results != null) {
                        Result result = results.get(0);
                        if (result != null) {
                            resultPlate = result.getPlate();
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new PlateRecognizerException("Problem with external service PlateRecognizer", e);
        }
        return resultPlate;
    }
}
