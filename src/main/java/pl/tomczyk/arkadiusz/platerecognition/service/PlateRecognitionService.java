package pl.tomczyk.arkadiusz.platerecognition.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.PlateRecognizerException;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate.RegistrationPlateException;
import pl.tomczyk.arkadiusz.platerecognition.web.mapper.RegistrationPlateMapper;
import pl.tomczyk.arkadiusz.platerecognition.web.mapper.RegistrationPlateResponseMapper;
import pl.tomczyk.arkadiusz.platerecognition.web.model.PlateRecognizerRequestModel;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateModel;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateResponseModel;


@Service
public class PlateRecognitionService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PlateRecognitionService.class);

    @Autowired
    public RegistrationPlateService registrationPlateService;

    @Autowired
    public LocationService locationService;

    @Autowired
    public VehicleService vehicleService;

    @Autowired
    public PlateRecognizerService plateRecognizerService;

    @Autowired
    private RegistrationPlateResponseMapper registrationPlateResponseMapper;

    @Autowired
    private RegistrationPlateMapper registrationPlateMapper;


    //    public RegistrationPlateResponseModel recognition(RegistrationPlateModel registrationPlateModel, Resource resource) throws RegistrationPlateException {
    public RegistrationPlateResponseModel recognition(PlateRecognizerRequestModel plateRecognizerRequestModel) throws RegistrationPlateException {
        try {
            String recognizedNumbers = plateRecognizerService.plateRecognizer(plateRecognizerRequestModel.getResource());
            plateRecognizerRequestModel.setNumbers(recognizedNumbers);
        } catch (PlateRecognizerException e) {
            e.printStackTrace();
        }
        RegistrationPlateModel registrationPlateModel = registrationPlateMapper.fromRequest(plateRecognizerRequestModel);
        RegistrationPlateModel recognition = registrationPlateService.recognition(registrationPlateModel);
        return registrationPlateResponseMapper.toResponse(recognition);

    }


}
