package pl.tomczyk.arkadiusz.platerecognition.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.LocationNotFoundException;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.LocationEntity;
import pl.tomczyk.arkadiusz.platerecognition.dao.repositories.LocationRepository;
import pl.tomczyk.arkadiusz.platerecognition.web.mapper.LocationMapper;
import pl.tomczyk.arkadiusz.platerecognition.web.model.LocationModel;

import java.util.Optional;

@Service
public class LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private LocationMapper locationMapper;

    public LocationModel create(LocationModel locationModel) {
        LocationEntity locationEntity = locationMapper.fromModel(locationModel);
        LocationEntity savedEntity = locationRepository.save(locationEntity);
        return locationMapper.fromEntity(savedEntity);
    }

    public LocationModel read(Long id) throws LocationNotFoundException {
        Optional<LocationEntity> optionalLocationEntity = locationRepository.findById(id);
        LocationEntity locationEntity = optionalLocationEntity.orElseThrow(() -> new LocationNotFoundException("unable to find location by Id: " + id));
        return locationMapper.fromEntity(locationEntity);
    }

}
