package pl.tomczyk.arkadiusz.platerecognition.service;

import org.springframework.stereotype.Service;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate.RegistrationPlateException;
import pl.tomczyk.arkadiusz.platerecognition.dao.entity.RegistrationPlateEntity;
import pl.tomczyk.arkadiusz.platerecognition.dao.repositories.RegistrationPlateRepository;
import pl.tomczyk.arkadiusz.platerecognition.web.mapper.RegistrationPlateMapper;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateModel;

import java.util.List;

@Service
public class RegistrationPlateService {

    private final RegistrationPlateRepository registrationPlateRepository;
    private final RegistrationPlateMapper registrationPlateMapper;

    public RegistrationPlateService(RegistrationPlateRepository registrationPlateRepository,
                                    RegistrationPlateMapper registrationPlateMapper) {
        this.registrationPlateRepository = registrationPlateRepository;
        this.registrationPlateMapper = registrationPlateMapper;
    }

    public List<RegistrationPlateModel> list() {
        List<RegistrationPlateEntity> registrationPlateEntities = registrationPlateRepository.findAll();
        return registrationPlateMapper.fromEntityList(registrationPlateEntities);
    }

    public RegistrationPlateModel recognition(RegistrationPlateModel registrationPlateModel)throws RegistrationPlateException {
        RegistrationPlateEntity registrationPlateEntity = registrationPlateMapper.fromModel(registrationPlateModel);
        RegistrationPlateEntity savedRegistrationPlateEntity = registrationPlateRepository.save(registrationPlateEntity);
        return registrationPlateMapper.fromEntity(savedRegistrationPlateEntity);
    }
}
