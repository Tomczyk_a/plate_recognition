package pl.tomczyk.arkadiusz.platerecognition.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

//@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PlateRecognitionRestControllerHttpRequestTest {

    private static final Logger LOGGER = Logger.getLogger(PlateRecognitionRestControllerHttpRequestTest.class.getName());

    @LocalServerPort // łączy randomowy port z url
    private int port;

    @Autowired
    private RestTemplate restTemplate;

    @Test
    public void recognition() {
            LOGGER.info(restTemplate + "");
        String forObject = restTemplate.getForObject("http://localhost:" + port + "/registration-plate", String.class);

        LOGGER.info(port + " info " );
        LOGGER.info(forObject + " news");

//        FIXME - dopisać asercje
    }
}