package pl.tomczyk.arkadiusz.platerecognition.web.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import pl.tomczyk.arkadiusz.platerecognition.service.PlateRecognitionService;
import pl.tomczyk.arkadiusz.platerecognition.service.RegistrationPlateService;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class PlateRecognitionRestControllerWebLayerMocksTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PlateRecognitionService plateRecognitionService;

    @MockBean
    private RegistrationPlateService registrationPlateService;

    @Test
    public void recognition() throws Exception {
        mockMvc.perform(get("http://localhost:8080/registration-plate"))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
