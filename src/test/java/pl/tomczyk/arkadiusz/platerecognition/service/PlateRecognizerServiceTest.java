package pl.tomczyk.arkadiusz.platerecognition.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.PlateRecognizerException;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PlateRecognizerServiceTest {

    private static final String NUMBERS = "71gnk8";

    @Autowired
    private PlateRecognizerService plateRecognizerService;

    @Test
    void plateRecognizer() throws PlateRecognizerException {
//        given
        Resource resource = new ClassPathResource("audi.jpg");
//        when
        String recognizedNumbers = plateRecognizerService.plateRecognizer(resource);
//        then
        assertEquals( NUMBERS , recognizedNumbers, "Registration plates numbers are not equal");

    }
}
