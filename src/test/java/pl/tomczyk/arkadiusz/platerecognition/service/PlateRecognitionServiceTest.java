package pl.tomczyk.arkadiusz.platerecognition.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate.RegistrationPlateException;
import pl.tomczyk.arkadiusz.platerecognition.web.model.PlateRecognizerRequestModel;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateResponseModel;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class PlateRecognitionServiceTest {

    private static final byte[] myvar = "my bytes".getBytes();

    @Autowired
    private PlateRecognitionService plateRecognitionService;

    @Test
    void recognition() throws RegistrationPlateException {
//        given

        Resource resource = new ClassPathResource("audi.jpg");
        PlateRecognizerRequestModel plateRecognizerRequestModel = new PlateRecognizerRequestModel(LocalDate.now(), resource,"sddf", myvar, "so1234" );
//        when
        RegistrationPlateResponseModel weirdStuff = plateRecognitionService.recognition(plateRecognizerRequestModel);
        String num  = weirdStuff.getNumbers();
//        then
        assertEquals(plateRecognizerRequestModel, weirdStuff, "do something");
    }
}