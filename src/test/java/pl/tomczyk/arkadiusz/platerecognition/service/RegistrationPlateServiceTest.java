package pl.tomczyk.arkadiusz.platerecognition.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import pl.tomczyk.arkadiusz.platerecognition.api.exception.registration.plate.RegistrationPlateException;
import pl.tomczyk.arkadiusz.platerecognition.web.model.RegistrationPlateModel;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class RegistrationPlateServiceTest {

    private static final String NUMBERS = "SO 85235";

    @Autowired
    private RegistrationPlateService registrationPlateService;

    @Test
    void recognition() throws RegistrationPlateException {
//        given
        RegistrationPlateModel registrationPlateModel = new RegistrationPlateModel(12L, NUMBERS, LocalDate.now(), null, null);
//        when
        RegistrationPlateModel recognizedRegistrationPlateModel = registrationPlateService.recognition(registrationPlateModel);
        String numbers = recognizedRegistrationPlateModel.getNumbers();

//        then
        assertEquals(NUMBERS, numbers, "Registration plates numbers are not equal");

    }
}